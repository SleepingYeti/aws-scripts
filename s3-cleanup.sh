# empties and deletes all S3 buckets that are not on a list of buckets to keep
# will not work if people have not followed AWS recommendations on bucket names (no spaces)

#!/bin/bash

profile="ben-admin"
  #"cdk-hnb659fds-assets-509594887342-eu-west-2" 
excluded=(
  "vf-ben-simpson-backups" 
  "vf-ben-simpson-sharing"
)

excludedFile=$(mktemp)
bucketsFile=$(mktemp)

aws --profile $profile s3api list-buckets | jq -r '.Buckets[].Name' > $bucketsFile

# thank you ChatGPT for helping me with emptying versioned buckets
empty_bucket() {
  BUCKET_NAME=$1
  echo "Emptying bucket: $BUCKET_NAME"

  # List all object versions and delete markers
  versions=$(aws --profile $profile s3api list-object-versions --bucket "$BUCKET_NAME" --query 'Versions[].{Key:Key,VersionId:VersionId}' --output json)
  delete_markers=$(aws --profile $profile s3api list-object-versions --bucket "$BUCKET_NAME" --query 'DeleteMarkers[].{Key:Key,VersionId:VersionId}' --output json)

  # Delete all versions
  echo "$versions" | jq -c '.[]' | while read -r version; do
    key=$(echo "$version" | jq -r '.Key')
    version_id=$(echo "$version" | jq -r '.VersionId')
    echo "Deleting version $version_id of $key"
    aws --profile $profile s3api delete-object --bucket "$BUCKET_NAME" --key "$key" --version-id "$version_id" > /dev/null
  done

  # Delete all delete markers
  echo "$delete_markers" | jq -c '.[]' | while read -r marker; do
    key=$(echo "$marker" | jq -r '.Key')
    version_id=$(echo "$marker" | jq -r '.VersionId')
    echo "Deleting delete marker $version_id of $key"
    aws --profile $profile s3api delete-object --bucket "$BUCKET_NAME" --key "$key" --version-id "$version_id" > /dev/null
  done
}

for item in "${excluded[@]}" ; do
  echo "$item" >> $excludedFile
done

cat $excludedFile
echo "----"
cat $bucketsFile
echo "----"
sort $bucketsFile -o $bucketsFile
sort $excludedFile -o $excludedFile

bucketsToDelete=$(comm -23 $bucketsFile $excludedFile)

echo $bucketsToDelete

for bucket in ${bucketsToDelete[@]} ; do
  #hopefully one of these commands will work for each bucket
  aws --profile $profile s3 rm s3://$bucket --recursive
  empty_bucket $bucket
    
  aws --profile $profile s3api delete-bucket --bucket $bucket
done
