#!/opt/homebrew/bin/zsh

# create a role in your developer account with the "Administrator_Access" managed policy attached
# give it a trust policy that allows "stsAssumeRole" acces to your normal developer role
# get command line credentials for your normal role from the access portal as usual

normal_profile="ben-creds"
admin_role_arn="arn:aws:iam::509594887342:role/develepor-elevated"

response=$(aws --profile $normal_profile sts assume-role --role-arn $admin_role_arn --role-session-name elevated-cli)

access_key_id=$(echo $response | jq -r '.Credentials.AccessKeyId')
secret_access_key=$(echo $response | jq -r '.Credentials.SecretAccessKey')
session_token=$(echo $response | jq -r '.Credentials.SessionToken')

echo "aws_access_key_id=$access_key_id"
echo "aws_secret_access_key=$secret_access_key"
echo "aws_session_token=$session_token"
